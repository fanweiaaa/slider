
//自执行匿名函数，避免全局变量污染
(function ($) {
    $.fn.yi_slider = function (options) {

        var defaults = {
            width:500,
            height:300
        }

        return this.each(function () {
            var $this = $(this);
            var $ctrl = $this.find(".ctrl-img"),
                $sliderImg = $this.find(".slider-img");

            $ctrl.click(function () {
                var index = 0;
                for (var i = 0; $ctrl.length; i++) {
                    if ($ctrl[i] === this) {
                        index = i;
                        break;
                    }
                }
                $sliderImg.fadeOut();
                $($sliderImg[index]).fadeIn();
            });
        });
    }
})(jQuery);





